﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Text;
using System.Linq;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;

// Statistics
using MathNet.Numerics.Statistics;

// Excel
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

using core = IntelSystems.Framework.Core;

namespace simulador_de_chuva
{
    public partial class MainForm: Form, core.IAppServices
    {
        core.IAppServices serviços;

        public MainForm()
        {
            InitializeComponent();

            serviços = this as core.IAppServices;
            serviços.LogLine("Aplicação iniciada");

            #if (DEBUG)
                // as caixas de texto manterao seus valores iniciais
            #else
                txtBartlettLambda.Text = "";
                txtBartlettNi.Text     = "";
                txtBartlettCapa.Text   = "";
                txtBartlettMiX.Text    = "";
                txtBartlettAlfa.Text   = "";
                txtBartlettPhi.Text    = "";

                txtNewmanLambda.Text   = "";
                txtNewmanBeta.Text     = "";
                txtNewmanEta.Text      = "";
                txtNewmanNi.Text       = "";
                txtNewmanKsi.Text      = "";
            #endif

            UpdateEditBoxes();
        }

        #region core.IAppServices

        object core.IAppServices.Get(string id)
        {
            throw new NotImplementedException();
        }

        void core.IAppServices.Log(string message, string color, bool logOnFile, bool forceLog)
        {
            output.SelectionColor = Color.FromName(color);
            output.AppendText(message);
        }

        void core.IAppServices.LogLine(string message, string color, bool logOnFile, bool forceLog)
        {
            if (Environment.NewLine.Length == 2 && message.Contains(Environment.NewLine))
                message = message.Replace(Environment.NewLine, "\r");

            (this as core.IAppServices).Log(_ident + message + "\r", color, logOnFile, forceLog);
            output.ScrollToCaret();
        }

        void core.IAppServices.Notificate(string message, object param)
        {
            throw new NotImplementedException();
        }

        void core.IAppServices.NotificateViaOS(string title, string message)
        {
            throw new NotImplementedException();
        }

        static string _ident = "";
        void core.IAppServices.SetIdent(string ident)
        {
            _ident = ident;
        }

        void core.IAppServices.SetNotificationMessage(string message)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Eventos

        private void btnSimular_Click(object sender, EventArgs e)
        {
            // inicializacao

            output.Clear();
            Program.Parar = false;

            list_media     .Clear();
            list_dp        .Clear();
            list_cov       .Clear();
            list_var       .Clear();
            list_propSecos .Clear();

            LerDadosReais();
            LimparResultados();

            melhorRMS = double.MaxValue;
            txtSimulacao.Text = "";
            txtRMS.Text = "";

            LerParametros();

            int numIntervalos = core.Converter.ToInt(txtNumIntervalos.Text, 90);
            int i = 1;
            int n = core.Converter.ToInt(txtNumSimulacoes.Text, 1);

            var folder = "resultados";
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            Task.Run(() => Process.Start(GetExecutingDirectoryName() + "\\" + folder));

            // simulacoes

            while (i <= n)
            {
                Simular(numIntervalos);

                var RMS = Comparar();
                if (RMS < melhorRMS)
                {
                    melhorRMS = RMS;
                    txtRMS.Text = RMS.ToString("N4");
                    this.melhorDadosSimulados = new double[this.dadosSimulados.Length];
                    this.dadosSimulados.CopyTo(this.melhorDadosSimulados, 0);

                    if (rbSalvarMelhorRMS.Checked)
                    {
                        Print(i, RMS);
                        SalvarSimulacao(i);
                    }
                }

                if (rbSalvarTodas.Checked)
                {
                    Print(i, RMS);
                    SalvarSimulacao(i);
                }

                txtSimulacao.Text = (i++).ToString();

                Application.DoEvents();
                if (Program.Parar)
                    break;
            }

            SalvarEstaticas();
        }

        private void SalvarEstaticas()
        {
            var sb = new StringBuilder(2048);

            sb.Append("media"    ).Append(';');
            sb.Append("dp"       ).Append(';');
            sb.Append("cov"      ).Append(';');
            sb.Append("var"      ).Append(';');
            sb.Append("propsecos").Append('\n');

            sb.AppendLine();

            // estatisticas da simulacao i

            for (int i = 0; i < list_media.Count; i++)
            {
                sb.Append(list_media    [i].ToString("N6")).Append(';');
                sb.Append(list_dp       [i].ToString("N6")).Append(';');
                sb.Append(list_cov      [i].ToString("N6")).Append(';');
                sb.Append(list_var      [i].ToString("N6")).Append(';');
                sb.Append(list_propSecos[i].ToString("N6")).Append('\n');
            }

            sb.AppendLine();

            // media das estatisticas

            sb.Append(list_media    .Mean().ToString("N6")).Append(';');
            sb.Append(list_dp       .Mean().ToString("N6")).Append(';');
            sb.Append(list_cov      .Mean().ToString("N6")).Append(';');
            sb.Append(list_var      .Mean().ToString("N6")).Append(';');
            sb.Append(list_propSecos.Mean().ToString("N6"));

            File.WriteAllText($"resultados/estatisticas.csv", sb.ToString());
        }

        private void LerParametros()
        {
            if (rbBarlett.Checked)
            {
                if (Program.InDebugMode)
                    BartlettSimulator.Serviços = serviços;

                BartlettSimulator.λ = Convert.ToDouble(txtBartlettLambda.Text);
                BartlettSimulator.ν = Convert.ToDouble(txtBartlettNi.Text);
                BartlettSimulator.κ = Convert.ToDouble(txtBartlettCapa.Text);
                BartlettSimulator.μx = Convert.ToDouble(txtBartlettMiX.Text);
                BartlettSimulator.α = Convert.ToDouble(txtBartlettAlfa.Text);
                BartlettSimulator.ф = Convert.ToDouble(txtBartlettPhi.Text);
            }

            if (rbNewman.Checked)
            {
                if (Program.InDebugMode)
                    NeymanSimulator.Serviços = serviços;

                NeymanSimulator.λ = Convert.ToDouble(txtNewmanLambda.Text);
                NeymanSimulator.β = Convert.ToDouble(txtNewmanBeta.Text);
                NeymanSimulator.η = Convert.ToDouble(txtNewmanEta.Text);  // duracao
                NeymanSimulator.ν = Convert.ToDouble(txtNewmanNi.Text);
                NeymanSimulator.ξ = Convert.ToDouble(txtNewmanKsi.Text);
            }
        }

        private void Print(int i, double RMS)
        {
            serviços.LogLine("");
            serviços.LogLine("Simulação: " + i);
            serviços.LogLine("");

            serviços.LogLine("                         REAL       SIMULADO");
            serviços.LogLine("");
            serviços.LogLine($"media      --> {dr_media.ToString("N4").PadLeft(14)} {ds_media.ToString("N4").PadLeft(14)}", "white");
            serviços.LogLine($"dp         --> {dr_dp.ToString("N4").PadLeft(14)} {ds_dp.ToString("N4").PadLeft(14)}", "white");
            serviços.LogLine($"cov        --> {dr_cov.ToString("N4").PadLeft(14)} {ds_cov.ToString("N4").PadLeft(14)}", "white");
            serviços.LogLine($"var        --> {dr_var.ToString("N4").PadLeft(14)} {ds_var.ToString("N4").PadLeft(14)}", "white");
            serviços.LogLine($"propSecos  --> {dr_propSecos.ToString("N4").PadLeft(14)} {ds_propSecos.ToString("N4").PadLeft(14)}", "white");
            serviços.LogLine("");
            serviços.LogLine("RMS --> " + RMS.ToString("N4"), "white");
            serviços.LogLine("");
        }

        private void LimparResultados()
        {
            var folder = "resultados";
            if (Directory.Exists(folder))
            {
                serviços?.LogLine($"Limpando resultados anteriores");
                Application.DoEvents();
                var files = Directory.EnumerateFiles(folder);
                foreach (var f in files)
                    File.Delete(f);
            }
        }

#endregion

#region Simulacao

        private double[] dadosReais;
        private double[] dadosSimulados;
        private double[] dadosSimuladosLag1;
        private double[] melhorDadosSimulados;

        private void LerDadosReais()
        {
            dr_media = 0d;
            LerCSV();
            //LerExcel();
        }

        //private void LerExcel()
        //{
        //    string arquivo;
        //    if (rbBarlett.Checked)
        //        arquivo = "dados_bartlett.xlsx";
        //    else
        //        arquivo = "dados_neyman.xlsx";

        //    serviços?.LogLine($"Lendo dados do arquivol => /dados reais/{arquivo} ... Aguarde alguns segundos ...");
        //    Application.DoEvents();

        //    XSSFWorkbook workbook;
        //    using (var file = new FileStream($"dados reais/{arquivo}", FileMode.Open, FileAccess.Read))
        //    {
        //        workbook = new XSSFWorkbook(file);
        //    }

        //    ISheet sheet = workbook.GetSheetAt(0);
        //    dadosReais = new double[sheet.LastRowNum];

        //    for (int i = 0; i < sheet.LastRowNum; i++)
        //    {
        //        // null is when the row only contains empty cells 
        //        if (sheet.GetRow(i) != null) 
        //        {
        //            // le somente a 1. coluna
        //            dadosReais[i] = sheet.GetRow(i).GetCell(0).NumericCellValue;
        //        }
        //    }
        //}

        private void LerCSV()
        {
            //string arquivo = "dados_5_min.csv";
            serviços?.LogLine($"Lendo dados do arquivo => {txtDados.Text} ... Aguarde alguns segundos ...");
            Application.DoEvents();

            // o arquivo deve ter somente uma coluna
            string[] conteudo = File.ReadAllLines(txtDados.Text);
            dadosReais = new double[conteudo.Length];
            for (var i = 0; i < conteudo.Length; i++)
                this.dadosReais[i] = double.Parse(conteudo[i]);

            txtNumIntervalos.Text = conteudo.Length.ToString();
            serviços?.LogLine($"Lidos {conteudo.Length} valores");
        }

        private void Simular(int numIntervalosDe5Min)
        {
            if (rbBarlett.Checked)
            {
                var simulador = new BartlettSimulator(numIntervalosDe5Min);

                this.dadosSimulados = simulador.Simular();

                // cria uma copia dos valores deslocando o 1. elemento em 1 celula (lag 1) para calculo da co-Variancia
                this.dadosSimuladosLag1 = new double[dadosSimulados.Length];
                for (var i = 0; i < dadosSimulados.Length - 1; i++)
                    dadosSimuladosLag1[i + 1] = dadosSimulados[i];
            }

            if (rbNewman.Checked)
            {
                var simulador = new NeymanSimulator(numIntervalosDe5Min);

                this.dadosSimulados = simulador.Simular();

                // cria uma copia dos valores deslocando o 1. elemento em 1 celula (lag 1) para calculo da co-Variancia
                this.dadosSimuladosLag1 = new double[dadosSimulados.Length];
                for (var i = 0; i < dadosSimulados.Length - 1; i++)
                    dadosSimuladosLag1[i + 1] = dadosSimulados[i];
            }
        }

        private void MostrarValores()
        {
            // obtem o tamanho da menor série
            var n = Math.Min(dadosReais.Length, dadosSimulados.Length);

            // mostra os valores
            serviços.SetIdent("       ");
            for (var i = 0; i < n; i++)
            {
                var h = dadosReais[i].ToString("N4").PadRight(7);
                var s = dadosSimulados[i].ToString("N4").PadLeft(7);
                serviços?.LogLine(h + " x  " + s);
            }
            serviços.SetIdent("");
        }

        double dr_media      = 0d;
        double dr_dp         = 0d;
        double dr_cov        = 0d;
        double dr_var        = 0d;
        double dr_propSecos  = 0d;

        double ds_media      = 0d;
        double ds_dp         = 0d;
        double ds_cov        = 0d;
        double ds_var        = 0d;
        double ds_propSecos  = 0d;

        double melhorRMS;

        List<double> list_media     = new List<double>();
        List<double> list_dp        = new List<double>();
        List<double> list_cov       = new List<double>();
        List<double> list_var       = new List<double>();
        List<double> list_propSecos = new List<double>();

        private double Comparar()
        {
            // obtem o tamanho da menor série
            double intervalos = Math.Min(dadosReais.Length, dadosSimulados.Length);

            //Média
            //Desvio Padrão
            //Cov
            //Variância
            //Dias úmidos
            //Dias secos
            //Proporção de dias secos

            // calcula apenas a 1. vez
            if (dr_media == 0d)
            {
                // cria uma copia dos valores deslocando o 1. elemento em 1 celula (lag 1) para calculo da co-Variancia
                double[] dadosReaisLag1 = new double[dadosReais.Length];
                for (var i = 0; i < dadosReais.Length - 1; i++)
                    dadosReaisLag1[i + 1] = dadosReais[i];

                dr_media = ArrayStatistics.Mean(dadosReais);
                dr_dp = ArrayStatistics.StandardDeviation(dadosReais);
                dr_cov = ArrayStatistics.Covariance(dadosReais, dadosReaisLag1);
                dr_var = ArrayStatistics.Variance(dadosReais);
                dr_propSecos = dadosReais.Count(x => x == 0) / intervalos;
            }

            ds_media = ArrayStatistics.Mean(dadosSimulados);
            ds_dp = ArrayStatistics.StandardDeviation(dadosSimulados);
            ds_cov = ArrayStatistics.Covariance(dadosSimulados, dadosSimuladosLag1);
            ds_var = ArrayStatistics.Variance(dadosSimulados);
            ds_propSecos = dadosSimulados.Count(x => x == 0) / intervalos;

            var sum = RMQ(dr_media, ds_media) +
                      RMQ(dr_dp, ds_dp) +
                      RMQ(dr_cov, ds_cov) +
                      RMQ(dr_var, ds_var) +
                      RMQ(dr_propSecos, ds_propSecos);

            var rms = Math.Sqrt(sum / 5.0);

            return rms;
        }

        private double RMQ(double r, double s)
        {
            if (r == 0)
                return 0;
            else
                return Math.Pow((s - r) / r, 2.0);
        }

#endregion

#region Outros

        //private void ExportarDados(Simulador_Manu simulador)
        //{
        //    var sb = new StringBuilder();
        //    foreach (var t in simulador.Tormentas)
        //    {
        //        foreach (var c in t.Celulas)
        //        {
        //            sb.AppendLine(c.TIOC.ToString("N2") + ';' + c.Duracao.ToString("N2") + ';' + c.Intensidade.ToString("N2"));
        //        }
        //    }
        //    File.WriteAllText("simulacao.csv", sb.ToString());
        //}

        private void btnParar_Click(object sender, EventArgs e)
        {
            Program.Parar = true;
        }        

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (melhorDadosSimulados != null)
            {
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    SalvarMelhorResultado();
                    SalvarMelhorResultado(10);
                    SalvarMelhorResultado(15);
                    SalvarMelhorResultado(20);
                    SalvarMelhorResultado(30);
                    SalvarMelhorResultado(60);
                    SalvarMelhorResultado(60 * 02);
                    SalvarMelhorResultado(60 * 06);
                    SalvarMelhorResultado(60 * 12);
                    SalvarMelhorResultado(60 * 24);
                }
            }
        }

        private void SalvarMelhorResultado(int minutos)
        {
            var intervalos = minutos / 5;

            var sb = new StringBuilder();
            var contador = 0;
            var acumulador = 0d;
            foreach (var v in melhorDadosSimulados)
            {
                contador++;
                acumulador += v;
                if (contador == intervalos)
                {
                    sb.AppendLine(acumulador.ToString());
                    acumulador = 0;
                    contador = 0;
                }
            }
            var filename = Path.GetFileNameWithoutExtension(saveDialog.FileName) + "." + minutos + Path.GetExtension(saveDialog.FileName);
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(saveDialog.FileName), filename), sb.ToString());
        }

        private void SalvarMelhorResultado()
        {
            var sb = new StringBuilder();
            foreach (var v in melhorDadosSimulados)
            {
                sb.AppendLine(v.ToString());
            }
            File.WriteAllText(saveDialog.FileName, sb.ToString());
        }

        private void SalvarSimulacao(int simulacao)
        {
            // lista das estatisticas

            list_media    .Add(ds_media);
            list_dp       .Add(ds_dp);
            list_cov      .Add(ds_cov);
            list_var      .Add(ds_var);
            list_propSecos.Add(ds_propSecos);

            // arquivos

            var sb = new StringBuilder();
            foreach (var v in dadosSimulados)
            {
                sb.AppendLine(v.ToString());
            }

            sb.AppendLine();

            sb.AppendLine($"media;{dr_media.ToString("N4")};{ds_media.ToString("N4")}");
            sb.AppendLine($"dp;{dr_dp.ToString("N4")};{ds_dp.ToString("N4")}");
            sb.AppendLine($"cov;{dr_cov.ToString("N4")};{ds_cov.ToString("N4")}");
            sb.AppendLine($"var;{dr_var.ToString("N4")};{ds_var.ToString("N4")}");
            sb.AppendLine($"propSecos;{dr_propSecos.ToString("N4")};{ds_propSecos.ToString("N4")}");

            File.WriteAllText($"resultados/sim_{simulacao}.0005_min.csv", sb.ToString());

            SalvarSimulacao(simulacao, 10 * 01);
            SalvarSimulacao(simulacao, 15 * 01);
            SalvarSimulacao(simulacao, 20 * 01);
            SalvarSimulacao(simulacao, 30 * 01);
            SalvarSimulacao(simulacao, 60 * 01);
            SalvarSimulacao(simulacao, 60 * 02);
            SalvarSimulacao(simulacao, 60 * 06);
            SalvarSimulacao(simulacao, 60 * 12);
            SalvarSimulacao(simulacao, 60 * 24);
        }

        private void SalvarSimulacao(int simulacao, int minutos)
        {
            var intervalos = minutos / 5;

            var sb = new StringBuilder();
            var contador = 0;
            var acumulador = 0d;
            foreach (var v in dadosSimulados)
            {
                contador++;
                acumulador += v;
                if (contador == intervalos)
                {
                    sb.AppendLine(acumulador.ToString());
                    acumulador = 0;
                    contador = 0;
                }
            }
            File.WriteAllText($"resultados/sim_{simulacao}.{minutos.ToString().PadLeft(4, '0')}_min.csv", sb.ToString());
        }

        private void btnSelecionarArquivo_Click(object sender, EventArgs e)
        {
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                txtDados.Text = openDialog.FileName;
            }
        }

        private void rbBarlett_CheckedChanged(object sender, EventArgs e)
        {
            UpdateEditBoxes();
        }

        private void rbNewman_CheckedChanged(object sender, EventArgs e)
        {
            UpdateEditBoxes();
        }

        private void UpdateEditBoxes()
        {
            var barlettSelected = rbBarlett.Checked;

            txtBartlettLambda.Enabled = barlettSelected;
            txtBartlettNi.Enabled = barlettSelected;
            txtBartlettCapa.Enabled = barlettSelected;
            txtBartlettMiX.Enabled = barlettSelected;
            txtBartlettAlfa.Enabled = barlettSelected;
            txtBartlettPhi.Enabled = barlettSelected;

            var newmanSelected = rbNewman.Checked;

            txtNewmanLambda.Enabled = newmanSelected;
            txtNewmanBeta.Enabled = newmanSelected;
            txtNewmanEta.Enabled = newmanSelected;
            txtNewmanNi.Enabled = newmanSelected;
            txtNewmanKsi.Enabled = newmanSelected;
        }

        public static string GetExecutingDirectoryName()
        {
            var location = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            return new FileInfo(location.AbsolutePath).Directory.FullName;
        }

        private void cbDebug_CheckedChanged(object sender, EventArgs e)
        {
            Program.InDebugMode = cbDebug.Checked;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            output.Clear();
            LerParametros();

            var t = 0d;
            for (var i = 0; i < 1000; i++)
            {
                var x = NeymanSimulator.NegExp(NeymanSimulator.β);
                t = t + x;
            }

            var m = "NegExp => Média de 1000 valores => " + (t / 1000).ToString("N3");
            serviços.LogLine(m);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            output.Clear();
            LerParametros();

            var t = 0d;
            for (var i = 0; i < 1000; i++)
            {
                var x = NeymanSimulator.Poisson();
                t = t + x;
            }

            var m = "Poisson => Média de 1000 valores => " + (t / 1000).ToString("N3");
            serviços.LogLine(m);
        }
    }

#endregion
}
