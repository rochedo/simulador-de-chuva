﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;

using dist = MathNet.Numerics.Distributions;
using core = IntelSystems.Framework.Core;

// (1 + 3 + 3 + 2 + 1) horas => pago
// (1 + 4 + 1 + 2 + 1) horas => pago

namespace simulador_de_chuva
{
    public class NeymanSimulator
    {
        #region static

        public static core.IAppServices Serviços;

        public static double λ ; // lambda (x1)
        public static double β ; // beta   (x2)
        public static double η ; // eta    (x3)
        public static double ν ; // ni     (x4)
        public static double ξ ; // ksi    (x5)

        public static double NegExp(double mean)
        {
            return Math.Log(u.NextDouble()) / (-mean);
        }

        public static double Poisson()
        {
            return p.Sample();
        }

        #endregion

        #region inner classes

        public class Tormenta
        {
            internal double TIT;
            internal List<Celula> Celulas;

            public Tormenta(double tit)
            {
                TIT = tit;
                CalcularCelulas();
                MostrarDados();
            }

            public double Intensidade
            {
                get
                {
                    double x = 0;
                    foreach (var c in Celulas)
                        x += c.Intensidade;
                    return x;
                }
            }

            //private void CalcularCelulas()
            //{ 
            //    double TIOC;

            //    TIOC = NegExp(β) + TIT;
            //    TFOC = Poisson() + TIT;

            //    Celulas = new List<Celula>();
            //    Celulas.Add(new Celula(TIOC));

            //    while (true)
            //    {
            //        TIOC = NegExp(β) + Celulas.Last().TIOC;
            //        if (TIOC < TFOC)
            //            Celulas.Add(new Celula(TIOC));
            //        else
            //            break;
            //    }
            //}

            private void CalcularCelulas()
            { 
                Celulas = new List<Celula>();
                var numeroDeCelulas = Poisson() + 1;
                for (int j = 1; j <= numeroDeCelulas; j++)
                {
                    var celula = new Celula();

                    celula.Inicio      = TIT + NegExp(β);
                    celula.Duracao     = NegExp(η);
                    celula.Intensidade = NegExp(ξ);
                    celula.Chunk       = (int) (celula.Inicio / 5);

                    Celulas.Add(celula);
                }
            }

            private void MostrarDados()
            { 
                Serviços?.LogLine($"η -> {η:N4} | Inicio da Tormenta -> {TIT:N2}");
                Serviços?.LogLine($"");
                Serviços?.SetIdent("    ");
                foreach (var c in Celulas)
                {
                    Serviços?.LogLine($"Inicio -> {c.Inicio:N2} ({c.Chunk}) | Duracao -> {c.Duracao:N4} | Intensidade -> {c.Intensidade:N2}");
                }
                Serviços?.LogLine("");
                Serviços?.LogLine("Intensidade da tormenta -> " + Intensidade.ToString("N2"), "yellow");
                Serviços?.LogLine("");
                Serviços?.SetIdent("  ");
            }
        }

        public class Celula
        {
            internal double Inicio;           
            internal double Duracao;
            internal double Intensidade;
            internal int    Chunk;

            internal Celula(double inicio)
            {
                Inicio      = inicio;
                Duracao     = NegExp(η);
                Intensidade = NegExp(1/ξ);
                Chunk       = (int) (Inicio / 5);
            }

            internal Celula()
            {
            }
        }

        #endregion

        public NeymanSimulator(int numIntervalos)
        {
            u = new Random();
            p = new dist.Poisson(ν, u);
            this.numIntervalos = numIntervalos;
        }

        public NeymanSimulator(int sementeDoGNA, int numIntervalos)
        {
            u = new Random(sementeDoGNA);
            p = new dist.Poisson(ν, u);
            this.numIntervalos = numIntervalos;
        }

        public List<Tormenta> Tormentas = new List<Tormenta>();

        public double[] Simular()
        {
            Serviços?.SetIdent("  ");

            // calcula o numero total de minutos
            //var numMinutosTotais = numDias * 24 * 60;
            var numMinutosTotais = numIntervalos * 5;

            // calcula as tormentas
            var tit = 0d;
            Tormentas.Add(new Tormenta(tit));
            while (true)
            {
                tit = NegExp(λ) + Tormentas.Last().TIT;
                if (tit < numMinutosTotais && !Program.Parar)
                {
                    // a ultima tormenta gerada podera gerar valores alem do limite de "numMinutosTotais"
                    // simplesmente desconsideramos estes valores ao agrupa-los. 
                    Tormentas.Add(new Tormenta(tit));

                    // avalia os eventos
                    Application.DoEvents();
                }
                else
                {
                    break;
                }
            }

            Serviços?.LogLine("Intensidade total simulada -> " + IntensidadeDasTormentas.ToString("N2"), "yellow");
            Serviços?.SetIdent("");

            Serviços?.LogLine("");
            Serviços?.LogLine($"Valores agrupados em intervalos de {tamIntervalo} minutos", "yellow");
            Serviços?.LogLine("");

            var numGrupos = numMinutosTotais / tamIntervalo;
            var valores = AgruparValores(numGrupos);
            //for (var i = 0; i < numGrupos; i++)
            //{
            //    Serviços?.LogLine($"{i.ToString().PadLeft(10)} -> {valores[i].ToString("N2").PadLeft(10)}");
            //}

            return valores;
        }

        public double IntensidadeDasTormentas
        {
            get
            {
                double x = 0;
                foreach (var t in Tormentas)
                    x = x + t.Intensidade;
                return x;
            }
        }

        #region private

        // numero de dias a serem simulados
        private double[] AgruparValores(int intervalos)
        {
            var array = new double[intervalos];
            for (var i = 0; i < array.Length; i++)
                array[i] = 0;

            foreach (var t in Tormentas)
            {
                foreach (var c in t.Celulas)
                {
                    if (c.Chunk < array.Length)
                        array[c.Chunk] += c.Intensidade;
                }
            }

            return array;
        }

        private readonly int numIntervalos;

        // tamanho do intervalo em minutos
        private readonly int tamIntervalo = 5;           
        
        // geradores aleatorios 
        private static Random u = new Random();
        private static dist.Poisson p = new dist.Poisson(5.01, u);

        #endregion
    }
}