﻿using System;
using System.Collections.Generic;
using System.Linq;

using dist = MathNet.Numerics.Distributions;
using core = IntelSystems.Framework.Core;
using System.Windows.Forms;

// (30) horas

// λ  -> Lambda
// ν  -> Nu
// κ  -> Kappa 
// μx -> Mu x
// α  -> Alfa
// ф  -> Phi
// η  -> Eta

namespace simulador_de_chuva
{
    public class BartlettSimulator
    {
        #region static

        public static core.IAppServices Serviços;

        public static double λ ;
        public static double ν ;
        public static double κ ;
        public static double μx;
        public static double α ;
        public static double ф ;

        public static double NegExp(double mean)
        {
            return Math.Log(u.NextDouble()) / (-mean);
        }

        public static double Gamma()
        {
            return g.Sample();
        }

        #endregion

        #region inner classes

        public class Tormenta
        {
            internal double TIT;
            internal double TFOC;
            internal List<Celula> Celulas;
            private readonly double η;

            public Tormenta(double tit)
            {
                TIT = tit;
                η = Gamma();
                CalcularCelulas();
                MostrarDados();
            }

            public double Intensidade
            {
                get
                {
                    double x = 0;
                    foreach (var c in Celulas)
                        x += c.Intensidade;
                    return x;
                }
            }

            private void CalcularCelulas()
            {
                double TIOC;

                TIOC = NegExp(κ * η) + TIT;
                TFOC = NegExp(ф * η) + TIT;

                Celulas = new List<Celula>();
                Celulas.Add(new Celula(η, TIOC));
                while (true)
                {
                    TIOC = NegExp(κ * η) + Celulas.Last().TIOC;
                    if (TIOC < TFOC)
                        Celulas.Add(new Celula(η, TIOC));
                    else
                        break;
                }
            }

            private void MostrarDados()
            { 
                Serviços?.LogLine($"η -> {η:N4} | TIT -> {TIT:N2} | TFOC -> {TFOC:N2}");
                Serviços?.LogLine($"");
                Serviços?.SetIdent("    ");
                foreach (var c in Celulas)
                {
                    Serviços?.LogLine($"TIOC -> {c.TIOC:N2} ({c.Chunk}) | Duracao -> {c.Duracao:N4} | Intensidade -> {c.Intensidade:N4}");
                }
                Serviços?.LogLine("");
                Serviços?.LogLine("Intensidade da tormenta -> " + Intensidade.ToString("N4"), "yellow");
                Serviços?.LogLine("");
                Serviços?.SetIdent("  ");
            }
        }

        public class Celula
        {
            internal double TIOC;           
            internal double Duracao;
            internal double Intensidade;
            internal int    Chunk;

            internal Celula(double η, double tioc)
            {
                TIOC        = tioc;
                Duracao     = NegExp(η);
                Intensidade = NegExp(1/μx);
                Chunk       = (int) (TIOC / 5);
            }
        }

        #endregion

        public BartlettSimulator(int numIntervalos)
        {
            u = new Random();
            g = new dist.Gamma(α, ν, u);
            this.numIntervalos = numIntervalos;
        }

        public BartlettSimulator(int sementeDoGNA, int numIntervalos)
        {
            u = new Random(sementeDoGNA);
            g = new dist.Gamma(α, ν, u);
            this.numIntervalos = numIntervalos;
        }

        public List<Tormenta> Tormentas = new List<Tormenta>();

        public double[] Simular()
        {
            Serviços?.SetIdent("  ");

            // calcula o numero total de minutos
            //var numMinutosTotais = numIntervalos * 24 * 60;
            var numMinutosTotais = this.numIntervalos * 5;

            // calcula as tormentas
            var tit = 0d;
            Tormentas.Add(new Tormenta(tit));
            while (true)
            {
                tit = NegExp(λ) + Tormentas.Last().TIT;
                if (tit < numMinutosTotais && !Program.Parar)
                {
                    // a ultima tormenta gerada podera gerar valores alem do limite de "numMinutosTotais"
                    // simplesmente desconsideramos estes valores ao agrupa-los. 
                    Tormentas.Add(new Tormenta(tit));

                    // avalia os eventos
                    Application.DoEvents();
                }
                else
                {
                    break;
                }
            }

            Serviços?.LogLine("Intensidade total simulada -> " + IntensidadeDasTormentas.ToString("N2"), "yellow");
            Serviços?.SetIdent("");

            Serviços?.LogLine("");
            Serviços?.LogLine($"Valores agrupados em intervalos de {tamIntervalo} minutos", "yellow");
            Serviços?.LogLine("");

            var numGrupos = numMinutosTotais / tamIntervalo;
            var valores = AgruparValores(numGrupos);
            //for (var i = 0; i < numGrupos; i++)
            //{
            //    Serviços?.LogLine($"{i.ToString().PadLeft(10)} -> {valores[i].ToString("N2").PadLeft(10)}");
            //}

            return valores;
        }

        public double IntensidadeDasTormentas
        {
            get
            {
                double x = 0;
                foreach (var t in Tormentas)
                    x = x + t.Intensidade;
                return x;
            }
        }

        #region private

        // numero de dias a serem simulados
        private double[] AgruparValores(int intervalos)
        {
            var array = new double[intervalos];
            for (var i = 0; i < array.Length; i++)
                array[i] = 0;

            foreach (var t in Tormentas)
            {
                foreach (var c in t.Celulas)
                {
                    if (c.Chunk < array.Length)
                        array[c.Chunk] += c.Intensidade;
                }
            }

            return array;
        }

        private readonly int numIntervalos;

        // tamanho do intervalo em minutos
        private readonly int tamIntervalo = 5;           
        
        private static Random u;
        private static dist.Gamma g;

        #endregion
    }
}