﻿namespace simulador_de_chuva
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbNewman = new System.Windows.Forms.RadioButton();
            this.rbBarlett = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbDebug = new System.Windows.Forms.CheckBox();
            this.rbSalvarMelhorRMS = new System.Windows.Forms.RadioButton();
            this.rbSalvarTodas = new System.Windows.Forms.RadioButton();
            this.txtNumSimulacoes = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnSelecionarArquivo = new System.Windows.Forms.Button();
            this.txtDados = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNumIntervalos = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnParar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtSimulacao = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRMS = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnSimular = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNewmanKsi = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNewmanNi = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNewmanEta = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNewmanBeta = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNewmanLambda = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBartlettPhi = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBartlettAlfa = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBartlettMiX = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBartlettCapa = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBartlettNi = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBartlettLambda = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.output = new System.Windows.Forms.RichTextBox();
            this.saveDialog = new System.Windows.Forms.SaveFileDialog();
            this.openDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.button3);
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox4);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.btnParar);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Controls.Add(this.btnSimular);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitContainer1.Panel1MinSize = 394;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.output);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(10);
            this.splitContainer1.Size = new System.Drawing.Size(1152, 861);
            this.splitContainer1.SplitterDistance = 394;
            this.splitContainer1.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkCyan;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(116, 795);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 25);
            this.button3.TabIndex = 17;
            this.button3.Text = "Poisson";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkCyan;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(10, 795);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 25);
            this.button2.TabIndex = 16;
            this.button2.Text = "NegExp";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbNewman);
            this.groupBox4.Controls.Add(this.rbBarlett);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.DarkCyan;
            this.groupBox4.Location = new System.Drawing.Point(11, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(356, 75);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Modelo";
            // 
            // rbNewman
            // 
            this.rbNewman.AutoSize = true;
            this.rbNewman.Checked = true;
            this.rbNewman.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNewman.ForeColor = System.Drawing.Color.Black;
            this.rbNewman.Location = new System.Drawing.Point(34, 43);
            this.rbNewman.Name = "rbNewman";
            this.rbNewman.Size = new System.Drawing.Size(236, 20);
            this.rbNewman.TabIndex = 1;
            this.rbNewman.TabStop = true;
            this.rbNewman.Text = "Neyman-Scott do Pulso Retangular";
            this.rbNewman.UseVisualStyleBackColor = true;
            this.rbNewman.CheckedChanged += new System.EventHandler(this.rbNewman_CheckedChanged);
            // 
            // rbBarlett
            // 
            this.rbBarlett.AutoSize = true;
            this.rbBarlett.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbBarlett.ForeColor = System.Drawing.Color.Black;
            this.rbBarlett.Location = new System.Drawing.Point(34, 21);
            this.rbBarlett.Name = "rbBarlett";
            this.rbBarlett.Size = new System.Drawing.Size(300, 20);
            this.rbBarlett.TabIndex = 0;
            this.rbBarlett.Text = "Bartlett-Lewis do Pulso Retangular Modificado";
            this.rbBarlett.UseVisualStyleBackColor = true;
            this.rbBarlett.CheckedChanged += new System.EventHandler(this.rbBarlett_CheckedChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkCyan;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(10, 754);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(358, 25);
            this.button1.TabIndex = 14;
            this.button1.Text = "Salvar simulação com melhor RMS";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbDebug);
            this.groupBox2.Controls.Add(this.rbSalvarMelhorRMS);
            this.groupBox2.Controls.Add(this.rbSalvarTodas);
            this.groupBox2.Controls.Add(this.txtNumSimulacoes);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.btnSelecionarArquivo);
            this.groupBox2.Controls.Add(this.txtDados);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtNumIntervalos);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.DarkCyan;
            this.groupBox2.Location = new System.Drawing.Point(11, 408);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(356, 201);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parâmetros da Simulação";
            // 
            // cbDebug
            // 
            this.cbDebug.AutoSize = true;
            this.cbDebug.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDebug.ForeColor = System.Drawing.Color.Black;
            this.cbDebug.Location = new System.Drawing.Point(19, 170);
            this.cbDebug.Name = "cbDebug";
            this.cbDebug.Size = new System.Drawing.Size(256, 20);
            this.cbDebug.TabIndex = 14;
            this.cbDebug.Text = "Mostrar cálculos intermediários na tela";
            this.cbDebug.UseVisualStyleBackColor = true;
            this.cbDebug.CheckedChanged += new System.EventHandler(this.cbDebug_CheckedChanged);
            // 
            // rbSalvarMelhorRMS
            // 
            this.rbSalvarMelhorRMS.AutoSize = true;
            this.rbSalvarMelhorRMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSalvarMelhorRMS.ForeColor = System.Drawing.Color.Black;
            this.rbSalvarMelhorRMS.Location = new System.Drawing.Point(18, 146);
            this.rbSalvarMelhorRMS.Name = "rbSalvarMelhorRMS";
            this.rbSalvarMelhorRMS.Size = new System.Drawing.Size(310, 20);
            this.rbSalvarMelhorRMS.TabIndex = 13;
            this.rbSalvarMelhorRMS.Text = "Salvar apenas as simulacoes com melhor RMS";
            this.rbSalvarMelhorRMS.UseVisualStyleBackColor = true;
            // 
            // rbSalvarTodas
            // 
            this.rbSalvarTodas.AutoSize = true;
            this.rbSalvarTodas.Checked = true;
            this.rbSalvarTodas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSalvarTodas.ForeColor = System.Drawing.Color.Black;
            this.rbSalvarTodas.Location = new System.Drawing.Point(18, 122);
            this.rbSalvarTodas.Name = "rbSalvarTodas";
            this.rbSalvarTodas.Size = new System.Drawing.Size(192, 20);
            this.rbSalvarTodas.TabIndex = 12;
            this.rbSalvarTodas.TabStop = true;
            this.rbSalvarTodas.Text = "Salvar todas as simulações";
            this.rbSalvarTodas.UseVisualStyleBackColor = true;
            // 
            // txtNumSimulacoes
            // 
            this.txtNumSimulacoes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumSimulacoes.Location = new System.Drawing.Point(93, 89);
            this.txtNumSimulacoes.Name = "txtNumSimulacoes";
            this.txtNumSimulacoes.Size = new System.Drawing.Size(77, 22);
            this.txtNumSimulacoes.TabIndex = 11;
            this.txtNumSimulacoes.Text = "1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(91, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 16);
            this.label15.TabIndex = 10;
            this.label15.Text = "Simulações";
            // 
            // btnSelecionarArquivo
            // 
            this.btnSelecionarArquivo.Location = new System.Drawing.Point(333, 40);
            this.btnSelecionarArquivo.Name = "btnSelecionarArquivo";
            this.btnSelecionarArquivo.Size = new System.Drawing.Size(21, 24);
            this.btnSelecionarArquivo.TabIndex = 9;
            this.btnSelecionarArquivo.Text = "...";
            this.btnSelecionarArquivo.UseVisualStyleBackColor = true;
            this.btnSelecionarArquivo.Click += new System.EventHandler(this.btnSelecionarArquivo_Click);
            // 
            // txtDados
            // 
            this.txtDados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDados.Location = new System.Drawing.Point(19, 41);
            this.txtDados.Name = "txtDados";
            this.txtDados.Size = new System.Drawing.Size(313, 22);
            this.txtDados.TabIndex = 8;
            this.txtDados.Text = "dados reais/dados_5_min.csv";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(17, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Dados reais";
            // 
            // txtNumIntervalos
            // 
            this.txtNumIntervalos.Enabled = false;
            this.txtNumIntervalos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumIntervalos.Location = new System.Drawing.Point(19, 89);
            this.txtNumIntervalos.Name = "txtNumIntervalos";
            this.txtNumIntervalos.ReadOnly = true;
            this.txtNumIntervalos.Size = new System.Drawing.Size(64, 22);
            this.txtNumIntervalos.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(17, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 16);
            this.label7.TabIndex = 5;
            this.label7.Text = "Intervalos";
            // 
            // btnParar
            // 
            this.btnParar.BackColor = System.Drawing.Color.DarkCyan;
            this.btnParar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnParar.ForeColor = System.Drawing.Color.White;
            this.btnParar.Location = new System.Drawing.Point(186, 618);
            this.btnParar.Name = "btnParar";
            this.btnParar.Size = new System.Drawing.Size(181, 25);
            this.btnParar.TabIndex = 12;
            this.btnParar.Text = "Parar";
            this.btnParar.UseVisualStyleBackColor = false;
            this.btnParar.Click += new System.EventHandler(this.btnParar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtSimulacao);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtRMS);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.DarkCyan;
            this.groupBox3.Location = new System.Drawing.Point(11, 655);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(356, 88);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " Resultados ";
            // 
            // txtSimulacao
            // 
            this.txtSimulacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSimulacao.Location = new System.Drawing.Point(136, 26);
            this.txtSimulacao.Name = "txtSimulacao";
            this.txtSimulacao.ReadOnly = true;
            this.txtSimulacao.Size = new System.Drawing.Size(84, 22);
            this.txtSimulacao.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(46, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 16);
            this.label13.TabIndex = 5;
            this.label13.Text = "Simulação n°";
            // 
            // txtRMS
            // 
            this.txtRMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRMS.Location = new System.Drawing.Point(194, 53);
            this.txtRMS.Name = "txtRMS";
            this.txtRMS.ReadOnly = true;
            this.txtRMS.Size = new System.Drawing.Size(84, 22);
            this.txtRMS.TabIndex = 4;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(46, 53);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(143, 16);
            this.label18.TabIndex = 3;
            this.label18.Text = "Melhor RMS até agora";
            // 
            // btnSimular
            // 
            this.btnSimular.BackColor = System.Drawing.Color.DarkCyan;
            this.btnSimular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSimular.ForeColor = System.Drawing.Color.White;
            this.btnSimular.Location = new System.Drawing.Point(11, 618);
            this.btnSimular.Name = "btnSimular";
            this.btnSimular.Size = new System.Drawing.Size(171, 25);
            this.btnSimular.TabIndex = 10;
            this.btnSimular.Text = "Simular";
            this.btnSimular.UseVisualStyleBackColor = false;
            this.btnSimular.Click += new System.EventHandler(this.btnSimular_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNewmanKsi);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtNewmanNi);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtNewmanEta);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtNewmanBeta);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtNewmanLambda);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtBartlettPhi);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtBartlettAlfa);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtBartlettMiX);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtBartlettCapa);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtBartlettNi);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtBartlettLambda);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.DarkCyan;
            this.groupBox1.Location = new System.Drawing.Point(11, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(356, 316);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Parâmetros do Modelo";
            // 
            // txtNewmanKsi
            // 
            this.txtNewmanKsi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewmanKsi.Location = new System.Drawing.Point(204, 231);
            this.txtNewmanKsi.Name = "txtNewmanKsi";
            this.txtNewmanKsi.Size = new System.Drawing.Size(84, 22);
            this.txtNewmanKsi.TabIndex = 17;
            this.txtNewmanKsi.Text = "4,62";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(202, 212);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 16);
            this.label9.TabIndex = 15;
            this.label9.Text = "ξ ( 1/mm/h )";
            // 
            // txtNewmanNi
            // 
            this.txtNewmanNi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewmanNi.Location = new System.Drawing.Point(204, 182);
            this.txtNewmanNi.Name = "txtNewmanNi";
            this.txtNewmanNi.Size = new System.Drawing.Size(84, 22);
            this.txtNewmanNi.TabIndex = 22;
            this.txtNewmanNi.Text = "5,01";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(202, 163);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 16);
            this.label10.TabIndex = 21;
            this.label10.Text = "ν";
            // 
            // txtNewmanEta
            // 
            this.txtNewmanEta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewmanEta.Location = new System.Drawing.Point(204, 135);
            this.txtNewmanEta.Name = "txtNewmanEta";
            this.txtNewmanEta.Size = new System.Drawing.Size(84, 22);
            this.txtNewmanEta.TabIndex = 20;
            this.txtNewmanEta.Text = "1,44";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(202, 116);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 16);
            this.label11.TabIndex = 19;
            this.label11.Text = "η ( 1/h )";
            // 
            // txtNewmanBeta
            // 
            this.txtNewmanBeta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewmanBeta.Location = new System.Drawing.Point(204, 90);
            this.txtNewmanBeta.Name = "txtNewmanBeta";
            this.txtNewmanBeta.Size = new System.Drawing.Size(84, 22);
            this.txtNewmanBeta.TabIndex = 18;
            this.txtNewmanBeta.Text = "1,55";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(202, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 16;
            this.label12.Text = "β  ( 1/h  )";
            // 
            // txtNewmanLambda
            // 
            this.txtNewmanLambda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewmanLambda.Location = new System.Drawing.Point(204, 45);
            this.txtNewmanLambda.Name = "txtNewmanLambda";
            this.txtNewmanLambda.Size = new System.Drawing.Size(84, 22);
            this.txtNewmanLambda.TabIndex = 14;
            this.txtNewmanLambda.Text = "0,00595";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(202, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 16);
            this.label14.TabIndex = 13;
            this.label14.Text = "λ ( 1/h )";
            // 
            // txtBartlettPhi
            // 
            this.txtBartlettPhi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBartlettPhi.Location = new System.Drawing.Point(80, 280);
            this.txtBartlettPhi.Name = "txtBartlettPhi";
            this.txtBartlettPhi.Size = new System.Drawing.Size(84, 22);
            this.txtBartlettPhi.TabIndex = 12;
            this.txtBartlettPhi.Text = "0,00253";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(78, 261);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "ф  ( 1/h )";
            // 
            // txtBartlettAlfa
            // 
            this.txtBartlettAlfa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBartlettAlfa.Location = new System.Drawing.Point(80, 231);
            this.txtBartlettAlfa.Name = "txtBartlettAlfa";
            this.txtBartlettAlfa.Size = new System.Drawing.Size(84, 22);
            this.txtBartlettAlfa.TabIndex = 6;
            this.txtBartlettAlfa.Text = "2,663";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(78, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "α ( 1/h )";
            // 
            // txtBartlettMiX
            // 
            this.txtBartlettMiX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBartlettMiX.Location = new System.Drawing.Point(80, 182);
            this.txtBartlettMiX.Name = "txtBartlettMiX";
            this.txtBartlettMiX.Size = new System.Drawing.Size(84, 22);
            this.txtBartlettMiX.TabIndex = 10;
            this.txtBartlettMiX.Text = "0,0124";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(78, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "μx ( 1/mm/h )";
            // 
            // txtBartlettCapa
            // 
            this.txtBartlettCapa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBartlettCapa.Location = new System.Drawing.Point(80, 135);
            this.txtBartlettCapa.Name = "txtBartlettCapa";
            this.txtBartlettCapa.Size = new System.Drawing.Size(84, 22);
            this.txtBartlettCapa.TabIndex = 8;
            this.txtBartlettCapa.Text = "0,157";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(78, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "κ";
            // 
            // txtBartlettNi
            // 
            this.txtBartlettNi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBartlettNi.Location = new System.Drawing.Point(80, 90);
            this.txtBartlettNi.Name = "txtBartlettNi";
            this.txtBartlettNi.Size = new System.Drawing.Size(84, 22);
            this.txtBartlettNi.TabIndex = 6;
            this.txtBartlettNi.Text = "0,440";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(78, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "ν";
            // 
            // txtBartlettLambda
            // 
            this.txtBartlettLambda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBartlettLambda.Location = new System.Drawing.Point(80, 45);
            this.txtBartlettLambda.Name = "txtBartlettLambda";
            this.txtBartlettLambda.Size = new System.Drawing.Size(84, 22);
            this.txtBartlettLambda.TabIndex = 4;
            this.txtBartlettLambda.Text = "0,00492";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(78, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "λ ( 1/h )";
            // 
            // output
            // 
            this.output.BackColor = System.Drawing.Color.Black;
            this.output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.output.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.output.ForeColor = System.Drawing.Color.Lime;
            this.output.Location = new System.Drawing.Point(10, 10);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(734, 841);
            this.output.TabIndex = 0;
            this.output.Text = "";
            // 
            // saveDialog
            // 
            this.saveDialog.DefaultExt = "*.csv";
            this.saveDialog.Filter = "Arquivos CSV|*.csv";
            this.saveDialog.Title = "Salvar resultado da simulação";
            // 
            // openDialog
            // 
            this.openDialog.Filter = "Arquivos CSV|*.csv|Todos|*.*";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1152, 861);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simulador de Chuva 2.8";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox output;
        private System.Windows.Forms.SaveFileDialog saveDialog;
        private System.Windows.Forms.OpenFileDialog openDialog;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rbNewman;
        private System.Windows.Forms.RadioButton rbBarlett;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbSalvarMelhorRMS;
        private System.Windows.Forms.RadioButton rbSalvarTodas;
        private System.Windows.Forms.TextBox txtNumSimulacoes;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnSelecionarArquivo;
        private System.Windows.Forms.TextBox txtDados;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumIntervalos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnParar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtSimulacao;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtRMS;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnSimular;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNewmanKsi;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNewmanNi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtNewmanEta;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNewmanBeta;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNewmanLambda;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBartlettPhi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBartlettAlfa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBartlettMiX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBartlettCapa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBartlettNi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBartlettLambda;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbDebug;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

