﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace simulador_de_chuva
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        public static bool Parar;
        public static bool InDebugMode = false;
    }
}
